#include <iostream>
#include <string>

#define FORBIDDEN_NUM 8200
#define ERROR_STR "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"

using namespace std;

int add(int a, int b) {
	if (a + b == FORBIDDEN_NUM)
	{
		throw (string(ERROR_STR));
	}
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void)
{
	try
	{
		cout << add(8000, 200) << endl;
	}
	catch (string& errorString)
	{
		cout << errorString << endl;
	}

	try
	{
		cout << add(8000, 200) << endl;
	}
	catch (string& errorString)
	{
		cout << errorString << endl;
	}

	getchar();
	return 0;
}