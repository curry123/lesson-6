#include <iostream>

#define FORBIDDEN_NUM 8200
#define ERROR_CODE -1
#define OK_CODE 1
#define ERROR_STR "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"

using namespace std;

int add(int a, int b, int& result) {
	if (a + b == FORBIDDEN_NUM)
	{
		return ERROR_CODE;
	}
	result = a + b;
	return OK_CODE;
}

int  multiply(int a, int b, int& result) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		if (add(sum, a, sum) == ERROR_CODE)
		{
			return ERROR_CODE;
		}
	};
	result = sum;
	return OK_CODE;
}

int  pow(int a, int b, int& result) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		if (multiply(exponent, a, exponent) == ERROR_CODE)
		{
			return ERROR_CODE;
		}
	};
	result = exponent;
	return OK_CODE;
}

int main() 
{
	int result = 0;

	if (add(8000, 200, result) == ERROR_CODE)
	{
		cout << ERROR_STR << endl;
	}
	else
	{
		cout << result << endl;
	}

	if (multiply(82, 100, result) == ERROR_CODE)
	{
		cout << ERROR_STR << endl;
	}
	else
	{
		cout << result << endl;
	}

	getchar();
	return 0;
}
