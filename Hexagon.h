#pragma once

#include <iostream>
#include "Shape.h"
#include "mathUtils.h"

class Hexagon : public Shape {

public:
	Hexagon(std::string nam, std::string col, double side);
	void draw();
	double CalArea();
	double getSide();
	void setSide(double side);
private:
	double side;
};
