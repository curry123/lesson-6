#pragma once

#include <iostream>
#include "shape.h"
#include "mathUtils.h"

class Pentagon : public Shape {

public:
	Pentagon(std::string nam, std::string col, double side);
	void draw();
	double CalArea();
	double getSide();
	void setSide(double side);
private:
	double side;
};
