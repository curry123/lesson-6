#include "pentagon.h"
#include "shapeexception.h"

Pentagon::Pentagon(std::string nam, std::string col, double side) : Shape::Shape(col, nam)
{
	setSide(side);
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Side is " << getSide() << std::endl << "Area: " << CalArea() << std::endl;;
}

double Pentagon::CalArea()
{
	return mathUtils::CalPentagonArea(this->side);
}
double Pentagon::getSide()
{
	return this->side;
}
void Pentagon::setSide(double side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	this->side = side;
}
