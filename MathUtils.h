#pragma once

class mathUtils
{
public:
	static double CalPentagonArea(double side);
	static double CalHexagonArea(double side);
};
