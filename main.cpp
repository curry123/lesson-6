#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "Hexagon.h"
#include "pentagon.h"
#include <string>
#include "shapeException.h"
#include "inputException.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, side = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(col, nam, side);
	Pentagon pen(col, nam, side);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrhex = &hex;
	Shape *ptrpen = &pen;



	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', hexagon = 'h', pentagon = 't'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, t = pentagon" << std::endl;
		std::cin >> shapetype;
		if (std::cin.peek() != '\n')
		{
			std::cout << "Warning - Don't try to build more than one shape at once" << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (!rad)
				{
					throw inputException();
				}
				getchar();
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (!width || !height)
				{
					throw inputException();
				}
				getchar();
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (!width || !height)
				{
					throw inputException();
				}
				getchar();
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (!width || !height || !ang || !ang2)
				{
					throw inputException();
				}
				getchar();
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'h':
				std::cout << "enter color, name, side" << std::endl;
				std::cin >> col >> nam >> side;
				if (!side)
				{
					throw inputException();
				}
				getchar();
				hex.setColor(col);
				hex.setName(nam);
				hex.setSide(side);
				ptrhex->draw();
				break;
			case 't':
				std::cout << "enter color, name, side" << std::endl;
				std::cin >> col >> nam >> side;
				if (!side)
				{
					throw inputException();
				}
				getchar();
				pen.setColor(col);
				pen.setName(nam);
				pen.setSide(side);
				ptrpen->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch (inputException& e)
		{
			std::cout << e.what() << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (shapeException& e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



	system("pause");
	return 0;

}
