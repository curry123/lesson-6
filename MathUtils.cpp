#include "mathUtils.h"
#include <math.h>

double mathUtils::CalPentagonArea(double side)
{
	return pow(side, 2) * 1.72048;
}

double mathUtils::CalHexagonArea(double side)
{
	return pow(side, 2) * 2.59808;
}
