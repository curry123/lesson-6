#include "hexagon.h"
#include "shapeexception.h"

Hexagon::Hexagon(std::string nam, std::string col, double side) : Shape::Shape(col, nam)
{
	setSide(side);
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Side is " << getSide() << std::endl << "Area: " << CalArea() << std::endl;;
}

double Hexagon::CalArea()
{
	return mathUtils::CalHexagonArea(this->side);
}
double Hexagon::getSide()
{
	return this->side;
}
void Hexagon::setSide(double side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	this->side = side;
}
